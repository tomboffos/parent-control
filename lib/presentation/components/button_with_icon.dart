import 'package:flutter/material.dart';

GlobalKey<_ButtonIconState> globalKey = GlobalKey();
GlobalKey<_ButtonIconState> globalKey2 = GlobalKey();
GlobalKey<_ButtonIconState> globalKey3 = GlobalKey();

class ButtonIcon extends StatefulWidget {
  final text;
  final action;
  final Widget icon;
  final withNotification;
  final notificationOff;
  final notificationButtonPressAction;
  const ButtonIcon(
      {GlobalKey key,
      this.text,
      this.action,
      this.icon,
      this.withNotification,
      this.notificationOff,
      this.notificationButtonPressAction})
      : super(key: key);

  @override
  _ButtonIconState createState() => _ButtonIconState();
}

class _ButtonIconState extends State<ButtonIcon> {
  int notificationOff;

  changeNotification() {
    print(notificationOff);
    setState(() {
      notificationOff == 1 ? notificationOff = 0 : notificationOff = 1;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    notificationOff = widget.notificationOff;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ButtonTheme(
            minWidth: MediaQuery.of(context).size.width - 120,
            height: 60,
            buttonColor: Colors.white,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  // borderRadius: BorderRadius.circular(20)
              ),
              onPressed: widget.action,
              color: Color(0xff0F7764),
              child: Container(
                width: MediaQuery.of(context).size.width / 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      widget.text,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),
                    ),
                    widget.icon
                  ],
                ),
              ),
            ),
          ),
          widget.withNotification
              ? GestureDetector(
                  onTap: widget.notificationButtonPressAction,
                  child: Container(
                    width: 55,
                    height: 55,
                    child: GestureDetector(
                      child: notificationOff == 0 ? Icon(Icons.notifications_off, color: Colors.white,): Icon(Icons.notifications, color: Colors.white,),
                      // onTap: changeNotification(),
                    ),
                    decoration: BoxDecoration(
                        // color: notificationOff == 0 ? Colors.red : Colors.green,
                        border: Border.all(color: Colors.white, width: 2),
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0, 0),
                              spreadRadius: 5,
                              blurRadius: 5,
                              color: Colors.black.withOpacity(0.1))
                        ],
                        borderRadius: BorderRadius.circular(40)
                    ),
                  ),
                )
              : SizedBox.shrink()
        ],
      ),
    );
  }
}
