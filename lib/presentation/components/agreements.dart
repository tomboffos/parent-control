import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Agreement extends StatelessWidget {
  const Agreement({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'при поддержке',
            style: GoogleFonts.montserrat(
                fontSize: 18,
                color: Color(0xffffffff),
                fontWeight: FontWeight.w700),
          ),
          Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/taraz.png'))),
          )
        ],
      ),
    );
  }
}
