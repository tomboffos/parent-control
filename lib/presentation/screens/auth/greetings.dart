import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:parent_control/cubit/parent/parent_cubit.dart';
import 'package:parent_control/presentation/components/agreements.dart';
import 'package:parent_control/presentation/components/button.dart';

class GreetingScreen extends StatefulWidget {
  @override
  _GreetingScreenState createState() => _GreetingScreenState();
}

class _GreetingScreenState extends State<GreetingScreen> {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ParentCubit>(context).checkLogged(context);
    return Scaffold(
      body: BlocBuilder<ParentCubit, ParentState>(
        builder: (context, state) {
          if (state is ParentLoginLoading)
            return Center(
              child: CircularProgressIndicator(),
            );

          return SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                  image: AssetImage('assets/images/father_and_child.png'),
                  alignment: Alignment.topCenter
                ),
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    Color(0xff488D9F),
                    Color(0xff2D3245),
                  ])
              ),
              padding:
                  EdgeInsets.only(top: MediaQuery.of(context).size.height / 25),
              // margin: EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width - 280,
                          height: MediaQuery.of(context).size.height / 2,
                          margin:
                              EdgeInsets.only(bottom: 30, top: 30),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/images/new_logo.png'),
                                fit: BoxFit.contain),
                            // borderRadius: BorderRadius.circular(30)
                          ),
                        ),

                          Text(
                            'Родительский\nконтроль',
                            style: GoogleFonts.montserrat(
                              color: Colors.white,
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                            textAlign: TextAlign.left,
                          ),
                      ],

                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 5,
                  ),
                  MainButton(
                    text: 'Войти',
                    route: '/login',
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 20,
                  ),
                  Agreement()
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
