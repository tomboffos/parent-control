import 'package:flutter/material.dart';

class ChooseButtons extends StatelessWidget {
  final firstText;
  final secondText;
  final firstAction;
  final secondAction;
  final width;

  const ChooseButtons(
      {Key key,
      this.firstText,
      this.secondText,
      this.firstAction,
      this.secondAction,
      this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width == null ? MediaQuery.of(context).size.width - 92 : width,
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xff005041)
        ),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              stops: [0.45, 0.55],
              colors: [Color(0xff005041), Color(0xff005041)])),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: firstAction,
            child: Container(
              child: Text(
                firstText,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
          GestureDetector(
            onTap: secondAction,
            child: Container(
              child: Text(
                secondText,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}
