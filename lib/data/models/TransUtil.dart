class TransUtil{
  String date;
  String iin;
  String type;
  String number;

  TransUtil.fromJson(Map json):
      date = json['date'],
      iin = json['iin'],
      type = json['type'],
      number = json['number'];
}