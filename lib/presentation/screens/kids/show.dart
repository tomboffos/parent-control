import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:parent_control/cubit/kid/kids_cubit.dart';
import 'package:parent_control/data/models/kid.dart';
import 'package:parent_control/presentation/components/button.dart';
import 'package:parent_control/presentation/components/button_with_icon.dart';

class KidIndex extends StatefulWidget {
  final Kid kid;

  const KidIndex({Key key, this.kid}) : super(key: key);

  @override
  _KidIndexState createState() => _KidIndexState();
}

class _KidIndexState extends State<KidIndex> {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<KidsCubit>(context).fetchKid(widget.kid.iin, true);
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocBuilder<KidsCubit, KidsState>(
        builder: (context, state) {
          if (!(state is KidLoaded))
            return Center(child: CircularProgressIndicator());

          final Kid kid = (state as KidLoaded).kid;
          return SingleChildScrollView(
            child: Container(
              // height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    Color(0xff488D9F),
                    Color(0xff2D3245),
                  ])),
              // margin:
              //     EdgeInsets.only(top: MediaQuery.of(context).size.height / 18),
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  Container(
                    width: 120,
                    height: 120,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image:         kid.gender == 1
                            ? AssetImage('assets/images/big_boy_avatar.png')
                            : AssetImage('assets/images/big_girl_avatar.png'),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(60.0)),
                      border: Border.all(
                        color: Colors.white,
                        width: 2.0
                      )
                    ),
                    // child: Center(
                    //   child: CircleAvatar(
                    //     foregroundImage:
                    //         kid.gender == 1
                    //             ? AssetImage('assets/images/big_boy_avatar.png')
                    //             : AssetImage('assets/images/big_girl_avatar.png'),
                    //         // AssetImage('assets/images/child_avatar.png'),
                    //     radius: 70,
                    //   ),
                    // ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    kid.fio,
                    style: GoogleFonts.montserrat(
                        fontSize: 24,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                  SizedBox(
                    height: 97,
                  ),
                  ButtonIcon(
                    key: globalKey2,
                    action: () => Navigator.pushNamed(context, '/kid/transport',
                        arguments: kid),
                    text: 'Транспорт',
                    icon: Icon(
                      Icons.emoji_transportation,
                      size: 30,
                      color: Colors.white,
                    ),
                    withNotification: true,
                    notificationOff: kid.trans_notif,
                    notificationButtonPressAction: () {
                      BlocProvider.of<KidsCubit>(context).updateNotification(
                          kid,
                          'trans_notif',
                          'updatetransnotif',
                          kid.trans_notif == 1 ? 0 : 1);
                      globalKey2.currentState.changeNotification();
                    },
                  ),
                  ButtonIcon(
                    key: globalKey,
                    action: () => Navigator.pushNamed(
                        context, '/kid/income-outcome',
                        arguments: kid),
                    text: 'Вход/Выход',
                    icon: Icon(
                      Icons.logout,
                      size: 30,
                      color: Colors.white,
                    ),
                    withNotification: true,
                    notificationOff: kid.turns_notif,
                    notificationButtonPressAction: () {
                      BlocProvider.of<KidsCubit>(context).updateNotification(
                          kid,
                          'turns_notif',
                          'updateturnsnotif',
                          kid.turns_notif == 1 ? 0 : 1);
                      globalKey.currentState.changeNotification();
                    },
                  ),
                  ButtonIcon(
                      key: globalKey3,
                      action: () => Navigator.pushNamed(context, '/kid/food',
                          arguments: kid),
                      text: 'Питание',
                      icon: Icon(
                        Icons.fastfood,
                        size: 30,
                        color: Colors.white,
                      ),
                      withNotification: true,
                      notificationOff: kid.food_notif,
                      notificationButtonPressAction: () {
                        BlocProvider.of<KidsCubit>(context).updateNotification(
                            kid,
                            'food_notif',
                            'updatefoodnotif',
                            kid.food_notif == 1 ? 0 : 1);
                        globalKey3.currentState.changeNotification();
                      }),
                  ButtonIcon(
                    action: () => Navigator.pushNamed(context, '/kid/report',
                        arguments: kid),
                    text: 'Хронология',
                    withNotification: false,
                    icon: Icon(
                      Icons.timer,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  MainButton(
                    text: 'К выбору ребенка',
                    action: () => Navigator.pop(context),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
