import 'package:flutter/material.dart';
import 'package:parent_control/data/models/kid.dart';
import 'package:parent_control/presentation/components/choose_buttons.dart';


class KidItem extends StatefulWidget {
  final Kid kid;
  final firstAction;
  final secondAction;
  const KidItem({Key key, this.kid, this.firstAction, this.secondAction})
      : super(key: key);

  @override
  _KidItemState createState() => _KidItemState();
}

class _KidItemState extends State<KidItem> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 25),
      child: Column(
        children: [
          GestureDetector(
            onLongPress: () {
              setState(() {
                selected = !selected;
              });
            },
            onTap: () =>
                Navigator.pushNamed(context, '/kid', arguments: widget.kid),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                  color: Color(0xff0F7764),
                  // boxShadow: [
                  //   BoxShadow(
                  //       offset: Offset(0, 0),
                  //       spreadRadius: 1,
                  //       blurRadius: 10,
                  //       color: Colors.grey),
                  // ],
                  // borderRadius: BorderRadius.circular(30)
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width / 3),
                    width: MediaQuery.of(context).size.width / 3,
                    child: Text(
                      widget.kid.fio,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),
                    ),
                  ),
                  Image(
                      image: widget.kid.gender == 1
                          ? AssetImage('assets/images/new_boy_avatar.png')
                          : AssetImage('assets/images/new_girl_avatar.png'),
                    // image: AssetImage('assets/images/child.png'),
                    width: 25,
                  )
                ],
              ),
            ),
          ),
          selected
              ? ChooseButtons(
                  width: MediaQuery.of(context).size.width - 150,
                  firstAction: widget.firstAction,
                  firstText: 'Изменить',
                  secondAction: widget.secondAction,
                  secondText: 'Удалить',
                )
              : SizedBox.shrink()
        ],
      ),
    );
  }
}
