class Kid{
  int id;
  String fio;
  int gender;
  String iin;
  int trans_notif;
  int turns_notif;
  int food_notif;

  Kid.fromJson(Map json):
      id = json['id'] as int,
      gender = json['gender'] as int,
      fio = json['fio'],
      iin = json['iin'],
      trans_notif = json['trans_notif'],
      turns_notif = json['turns_notif'],
      food_notif = json['food_notif'];

}