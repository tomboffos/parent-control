class Transport{
  DateTime date;
  String iin;
  String type;
  String number;
  String gos_number;

  Transport.fromJson(Map json):
      date = DateTime.parse(json['date']),
      iin = json['iin'],
      type = json['type'],
      number = json['number'],
      gos_number = json['gos_number'];

}