part of 'kids_cubit.dart';

@immutable
abstract class KidsState {}

class KidsInitial extends KidsState {}

class KidsLoaded extends KidsState {
  final List<Kid> kids;
  final bool close;

  KidsLoaded({this.kids,this.close});
}

class KidsLoading extends KidsState {}

class KidLoading extends KidsState {}

class KidLoaded extends KidsState {
  final Kid kid;

  KidLoaded({this.kid});
}

class KidUpdated extends KidsState {
  final Kid kid;

  KidUpdated({this.kid});
}

class AllLoading extends KidsState {}

class AllLodaded extends KidsState {
  final List<dynamic> reports;

  AllLodaded({this.reports});
}
