import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainField extends StatelessWidget {
  final placeholder;
  final keyBoardType;
  final suffixIcon;
  final controller;
  final obscureText;
  const MainField(
      {Key key,
      this.placeholder,
      this.keyBoardType,
      this.suffixIcon,
      this.controller,
      this.obscureText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: keyBoardType,
      controller: controller,

      obscureText: obscureText != null ? obscureText : false,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
        hintText: placeholder,
        suffixIcon: suffixIcon,
        hintStyle: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 2,
          ),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 2,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 2,
          ),


        ),

      ),
    );
  }
}
