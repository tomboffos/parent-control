part of 'foods_cubit.dart';

@immutable
abstract class FoodsState {}

class FoodsInitial extends FoodsState {}

class FoodsLoaded extends FoodsState {
  final List<Food> foods;

  FoodsLoaded({this.foods});
}

