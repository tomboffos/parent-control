part of 'transtiles_cubit.dart';

@immutable
abstract class TranstilesState {}

class TranstilesInitial extends TranstilesState {}

class TranstilesLoading extends TranstilesState {}

class TranstilesLoaded extends TranstilesState {
  final List<TransUtil> transUtils;

  TranstilesLoaded({this.transUtils});
}
