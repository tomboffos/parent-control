import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:parent_control/cubit/transtile/transtiles_cubit.dart';
import 'package:parent_control/data/models/kid.dart';
import 'package:parent_control/presentation/components/button.dart';

class IncomeIndex extends StatefulWidget {
  final Kid kid;

  const IncomeIndex({Key key, this.kid}) : super(key: key);

  @override
  _IncomeIndexState createState() => _IncomeIndexState();
}

class _IncomeIndexState extends State<IncomeIndex> {
  DateTime dateTime = new DateTime.now();
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<TranstilesCubit>(context)
        .fetchTranstiles(widget.kid.iin, dateTime);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          // height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xff488D9F),
                    Color(0xff2D3245),
                  ])),
          padding: EdgeInsets.symmetric(horizontal: 40),
          // margin: EdgeInsets.only(top: 100),
          child: Column(
            children: [
              SizedBox(
                height: 70,
              ),
              Row(
                children: [
                  Image(
                    image:
                    widget.kid.gender == 1
                        ? AssetImage('assets/images/new_boy_avatar.png')
                        : AssetImage('assets/images/new_girl_avatar.png'),
                    // AssetImage('assets/images/child_avatar.png'),
                    height: 95,
                    width: 95,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    widget.kid.fio,
                    style: GoogleFonts.montserrat(fontSize: 24, fontWeight: FontWeight.w600, color: Colors.white),
                  )
                ],
              ),
              SizedBox(
                height: 31,
              ),
              Container(
                padding:
                    EdgeInsets.only(top: 12, bottom: 43, left: 31, right: 31),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Color(0xff0F7764),
                    // boxShadow: [
                    //   BoxShadow(
                    //       offset: Offset(0, 0),
                    //       spreadRadius: 1,
                    //       blurRadius: 10,
                    //       color: Colors.grey),
                    // ],
                    // borderRadius: BorderRadius.circular(30)
                ),
                child: Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Вход/Выход',
                          style: GoogleFonts.montserrat(
                              fontSize: 24, fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '${dateTime.day}.${dateTime.month}.${dateTime.year}',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600, color: Colors.white),
                            ),
                            GestureDetector(
                              onTap: () => DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(2015, 3, 5),
                                  maxTime: DateTime.now(), onConfirm: (date) {
                                setState(() {
                                  dateTime = date;
                                });
                              }, currentTime: dateTime, locale: LocaleType.ru),
                              child: Text(
                                'Выбрать время',
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w600, color: Colors.white),
                              ),
                            )
                          ],
                        ),
                        Divider(
                          color: Colors.white,
                          thickness: 2,
                        ),
                        BlocBuilder<TranstilesCubit, TranstilesState>(
                          builder: (context, state) {
                            if (!(state is TranstilesLoaded))
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            final transUtils =
                                (state as TranstilesLoaded).transUtils;
                            return Column(
                                children: transUtils
                                    .map((transtile) => Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                '${transtile.type} ${transtile.number}',
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w600, color: Colors.white),
                                              ),
                                              Text(
                                                '${DateTime.parse(transtile.date).hour} : ${DateTime.parse(transtile.date).minute}',
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w600, color: Colors.white),
                                              ),
                                            ],
                                          ),
                                        ))
                                    .toList());
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 1.8,
              ),
              MainButton(
                text: 'На главную',
                action: () => Navigator.pop(context),
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
