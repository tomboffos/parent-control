import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:parent_control/data/models/Food.dart';
import 'package:parent_control/data/repository/repository.dart';

part 'foods_state.dart';

class FoodsCubit extends Cubit<FoodsState> {
  final Repository repository;

  FoodsCubit({this.repository}) : super(FoodsInitial());

  void fetchFood(String iin, DateTime dateTime) async {
    print(iin);
    repository
        .fetchFoods(iin, dateTime)
        .then((foods) => emit(FoodsLoaded(foods: foods)));
  }
}
