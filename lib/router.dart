
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parent_control/cubit/food/foods_cubit.dart';
import 'package:parent_control/cubit/kid/kids_cubit.dart';
import 'package:parent_control/cubit/parent/parent_cubit.dart';
import 'package:parent_control/cubit/transport/transport_cubit.dart';
import 'package:parent_control/cubit/transtile/transtiles_cubit.dart';
import 'package:parent_control/data/network_service.dart';
import 'package:parent_control/presentation/screens/auth/forget.dart';
import 'package:parent_control/presentation/screens/auth/greetings.dart';
import 'package:parent_control/presentation/screens/auth/login.dart';
import 'package:parent_control/presentation/screens/auth/register.dart';
import 'package:parent_control/presentation/screens/kids/food/index.dart';
import 'package:parent_control/presentation/screens/kids/income/index.dart';
import 'package:parent_control/presentation/screens/kids/index.dart';
import 'package:parent_control/presentation/screens/kids/report.dart';
import 'package:parent_control/presentation/screens/kids/show.dart';
import 'package:parent_control/presentation/screens/kids/transport/index.dart';

import 'data/repository/repository.dart';

class AppRouter {
  Repository repository;

  AppRouter() {
    repository = Repository(networkService: NetworkService());
  }

  Route generateRouter(RouteSettings settings) {
    final arguments = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => ParentCubit(repository: repository),
                  child: GreetingScreen(),
                ));
      case '/login':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => ParentCubit(repository: repository),
                  child: Login(),
                ));
      case '/register':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => ParentCubit(repository: repository),
                  child: RegisterPage(),
                ));
      case '/kids':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => KidsCubit(repository: repository),
                  child: KidsIndex(),
                ));
      case '/kid':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => KidsCubit(repository: repository),
                  child: KidIndex(
                    kid: arguments,
                  ),
                ));
      case '/kid/transport':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => TransportCubit(repository: repository),
                  child: TransportIndex(kid: arguments),
                ));
      case '/kid/income-outcome':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => TranstilesCubit(repository: repository),
                  child: IncomeIndex(
                    kid: arguments,
                  ),
                ));
      case '/kid/food':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => FoodsCubit(repository: repository),
                  child: FoodIndex(kid: arguments),
                ));
      case '/kid/report':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => KidsCubit(repository: repository),
                  child: Report(
                    kid: arguments,
                  ),
                ));
      case '/forget':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => ParentCubit(repository: repository),
                  child: ForgetPassword(),
                ));
      default:
        return null;
    }
  }
}
