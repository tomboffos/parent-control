import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:meta/meta.dart';
import 'package:parent_control/data/repository/repository.dart';

part 'parent_state.dart';

class ParentCubit extends Cubit<ParentState> {
  final Repository repository;

  ParentCubit({this.repository}) : super(ParentInitial());

  void login(String phone, String password, BuildContext context) {
    emit(ParentLoginLoading());
    print(phone); 
    repository.login(phone, password, context).then((parentId) {
      if (parentId != null) {
        emit(ParentLogged(parentId: parentId));
      } else {
        print(parentId);
        emit(ParentNotLogged());
      }
    });
  }

  void checkLogged(BuildContext context) {
    emit(ParentLoginLoading());

    repository.checkParentId(context).then((parentId) => parentId != null
        ? Navigator.pushNamedAndRemoveUntil(context, '/kids', (route) => false)
        : emit(ParentNotLogged()));
  }

  void register(BuildContext context, Map<String, dynamic> form) {
    emit(ParentLoginLoading());

    repository.register(context, form).then((parentId) => parentId != null
        ? emit(ParentLogged(parentId: parentId))
        : ParentNotLogged());
  }

  void requestPinCode(BuildContext context, TextEditingController email) {
    repository.requestPinCode(context, email);
  }

  void sendPinCode(BuildContext context, TextEditingController password,
      TextEditingController pincode) {
    repository.sendPinCode(context, password, pincode);
  }
}
