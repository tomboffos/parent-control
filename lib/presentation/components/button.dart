import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MainButton extends StatelessWidget {
  final String text;
  final String route;
  final action;

  const MainButton({Key key, this.text, this.route, this.action})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: MediaQuery.of(context).size.width - 74,
      height: 50,
      buttonColor: Color(0xffF8F8F9),
      child: RaisedButton(
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        onPressed:action != null ? action : () => Navigator.pushNamed(context, route),
        color: Color(0xff0F7764),
        child: Text(
          text,
          style: GoogleFonts.montserrat(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),

        ),
      ),
    );
  }
}
