import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:parent_control/cubit/food/foods_cubit.dart';
import 'package:parent_control/data/models/kid.dart';
import 'package:parent_control/presentation/components/button.dart';

class FoodIndex extends StatefulWidget {
  final Kid kid;

  const FoodIndex({Key key, this.kid}) : super(key: key);

  @override
  _FoodIndexState createState() => _FoodIndexState();
}

class _FoodIndexState extends State<FoodIndex> {
  DateTime dateTime = new DateTime.now();
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<FoodsCubit>(context).fetchFood(widget.kid.iin, dateTime);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xff488D9F),
                    Color(0xff2D3245),
                  ])),
          padding: EdgeInsets.symmetric(horizontal: 40),
          // margin: EdgeInsets.only(top: 100),
          child: Column(
            children: [
              SizedBox(
                height: 70,
              ),
              Row(
                children: [
                  Image(
                    image:
                    widget.kid.gender == 1
                        ? AssetImage('assets/images/new_boy_avatar.png')
                        : AssetImage('assets/images/new_girl_avatar.png'),
                    // AssetImage('assets/images/child_avatar.png'),
                    height: 95,
                    width: 95,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    widget.kid.fio,
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600, color: Colors.white),
                  )
                ],
              ),
              SizedBox(
                height: 31,
              ),
              Container(
                padding:
                EdgeInsets.only(top: 12, bottom: 43, left: 31, right: 31),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Color(0xff0F7764),
                  // boxShadow: [
                  //   BoxShadow(
                  //       offset: Offset(0, 0),
                  //       spreadRadius: 1,
                  //       blurRadius: 10,
                  //       color: Colors.grey),
                  // ],
                  // borderRadius: BorderRadius.circular(30)
                ),
                child: Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Питание',
                          style: GoogleFonts.montserrat(
                              fontSize: 24, fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '${dateTime.day}.${dateTime.month}.${dateTime.year}',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600, color: Colors.white),
                            ),
                            GestureDetector(
                              onTap: () => DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(2015, 3, 5),
                                  maxTime: DateTime.now(), onConfirm: (date) {
                                setState(() {
                                  dateTime = date;
                                });
                              }, currentTime: dateTime, locale: LocaleType.ru),
                              child: Text(
                                'Выбрать время',
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w600, color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.white,
                          thickness: 2,
                        ),
                        BlocBuilder<FoodsCubit, FoodsState>(
                          builder: (context, state) {
                            if (!(state is FoodsLoaded))
                              return Container(
                                margin: EdgeInsets.symmetric(vertical: 20),
                                child:
                                    Center(child: CircularProgressIndicator()),
                              );
                            final foods = (state as FoodsLoaded).foods;
                            return Column(
                              children: foods
                                  .map((food) => Container(
                                        margin: EdgeInsets.only(bottom: 10),
                                        child: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  'Счет за покупку',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w600, color: Colors.white),
                                                ),
                                                Text(
                                                  '${food.date.hour}:${food.date.minute}',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w600, color: Colors.white),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              children: food.type
                                                  .map(
                                                    (product) => Container(
                                                      margin: EdgeInsets.only(
                                                          left: 30),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text(
                                                            product['name'],
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600, color: Colors.white),
                                                          ),
                                                          Text(
                                                            '${product['price']} тг',
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600, color: Colors.white),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                  .toList(),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 10),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'ИТОГО',
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w600, color: Colors.white),
                                                  ),
                                                  Text(
                                                    '${food.type.fold(0, (i, product) => i + int.parse(product["price"])).toString()} тг',
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w600, color: Colors.white),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ))
                                  .toList(),
                            );
                          },
                        ),
                        // Column(
                        //   children: [
                        //     Container(
                        //       margin: EdgeInsets.only(bottom: 10),
                        //       child: Row(
                        //         mainAxisAlignment:
                        //             MainAxisAlignment.spaceBetween,
                        //         children: [
                        //           Text(
                        //             'ИТОГО:',
                        //             style: TextStyle(
                        //                 fontSize: 14,
                        //                 fontWeight: FontWeight.w600),
                        //           ),
                        //           Text(
                        //             '22 000 тг',
                        //             style: TextStyle(
                        //                 fontSize: 14,
                        //                 fontWeight: FontWeight.w600),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //   ],
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 1.8,
              ),
              MainButton(
                text: 'На главную',
                action: () => Navigator.pop(context),
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
