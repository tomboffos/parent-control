import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:parent_control/cubit/kid/kids_cubit.dart';
import 'package:parent_control/presentation/components/agreements.dart';
import 'package:parent_control/presentation/components/button.dart';
import 'package:parent_control/presentation/components/choose_buttons.dart';
import 'package:parent_control/presentation/components/field.dart';
import 'package:parent_control/presentation/items/kid.dart';
import 'package:shared_preferences/shared_preferences.dart';

class KidsIndex extends StatefulWidget {
  const KidsIndex({Key key}) : super(key: key);

  @override
  _KidsIndexState createState() => _KidsIndexState();
}

class _KidsIndexState extends State<KidsIndex> {
  bool showForm = false;
  String gender = '1';

  TextEditingController iin = new MaskedTextController(mask: '000000000000');
  TextEditingController name = new TextEditingController();
  String id;
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<KidsCubit>(context).fetchKids();
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          // height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xff488D9F),
                    Color(0xff2D3245),
                  ])),
          // margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 12),
          padding: EdgeInsets.only(left: 27, right: 27, top: 80),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 5,
                    height: MediaQuery.of(context).size.height / 5,
                    margin: EdgeInsets.only(bottom: 30, left: 45),
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/new_logo.png'),
                            fit: BoxFit.contain),
                        borderRadius: BorderRadius.circular(30)),
                  ),
                  Text(
                    'Родительский\nконтроль',
                    style: GoogleFonts.montserrat(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),

              SizedBox(
                height: 50,
              ),
              Text(
                'Выбор ребенка',
                style: GoogleFonts.montserrat(
                    fontSize: 24, fontWeight: FontWeight.w400, color: Colors.white),
              ),
              SizedBox(
                height: 20,
              ),
              BlocBuilder<KidsCubit, KidsState>(builder: (context, state) {
                if (!(state is KidsLoaded))
                  return Container(
                      margin: EdgeInsets.symmetric(vertical: 20),
                      child: CircularProgressIndicator());

                final kids = (state as KidsLoaded).kids;
                return Column(
                  children: kids
                      .map((kid) => KidItem(
                            kid: kid,
                            secondAction: () => {
                              BlocProvider.of<KidsCubit>(context).deleteKid(kid)
                            },
                            firstAction: () {
                              setState(() {
                                iin.text = kid.iin;
                                name.text = kid.fio;
                                id = kid.id.toString();
                                showForm = true;
                              });
                            },
                          ))
                      .toList(),
                );
                // return
              }),
              showForm
                  ? Container(
                      padding: EdgeInsets.only(
                          top: 12, bottom: 30, left: 20, right: 20),
                      margin: EdgeInsets.symmetric(horizontal: 18),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Color(0xff0F7764),
                          // boxShadow: [
                          //   BoxShadow(
                          //       offset: Offset(0, 0),
                          //       spreadRadius: 1,
                          //       blurRadius: 10,
                          //       color: Colors.grey),
                          // ],
                          borderRadius: BorderRadius.circular(30)
                      ),
                      child: Column(
                        children: [
                          Text(
                            id != null
                                ? 'Изменить ребенка'
                                : 'Добавить ребенка',
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: MainField(
                              placeholder: 'Имя',
                              controller: name,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: MainField(
                              placeholder: 'ИИН',
                              controller: iin,
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 8),
                            child: DropdownButton<String>(
                                value: gender,
                                isExpanded: true,
                                icon: const Icon(
                                  Icons.arrow_downward,
                                  color: Colors.white,
                                ),
                                iconSize: 24,
                                dropdownColor: Color(0xff0F7764),
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16),
                                underline: Container(
                                  height: 2,
                                  color: Colors.white,
                                ),
                                onChanged: (String newValue) {
                                  setState(() {
                                    gender = newValue;
                                  });
                                },
                                items: [
                                  DropdownMenuItem<String>(
                                    value: '1',
                                    child: Text(
                                      'Мальчик',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,),
                                    ),
                                  ),
                                  DropdownMenuItem<String>(
                                    value: '2',
                                    child: Text('Девочка'),
                                  ),
                                ]),
                          )
                        ],
                      ),
                    )
                  : ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width - 74,
                      height: 50,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0)),
                        onPressed: () {
                          setState(() {
                            showForm = true;
                            id = null;
                          });
                        },
                        color: Color(0xff0F7764),
                        child: Text(
                          id != null ? 'Изменить ребенка' : 'Добавить ребенка',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                      ),
                    ),
              showForm
                  ? ChooseButtons(
                      firstText: id != null ? 'Изменить' : 'Добавить',
                      firstAction: () {
                        BlocProvider.of<KidsCubit>(context).registerKid(
                            context, {
                          "iin": iin.text,
                          "name": name.text,
                          "gender": gender,
                          "id": id
                        }).then((value) {
                          setState(() {
                            id = null;
                            if (!value) {
                              iin.text = '';
                              name.text = '';
                              showForm = false;
                            }
                          });
                        });
                      },
                      secondText: 'Отменить',
                      secondAction: () {
                        BlocProvider.of<KidsCubit>(context).cancelAll();
                        setState(() {
                          iin.text = '';
                          name.text = '';
                          id = null;
                          showForm = false;
                        });
                      },
                      width: MediaQuery.of(context).size.width - 150,
                    )
                  : SizedBox.shrink(),
              SizedBox(
                height: 150,
              ),
              GestureDetector(
                onTap: () async {
                  (await SharedPreferences.getInstance()).remove('parentId');
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/', (route) => false);
                },
                child: Container(
                  child: Text('Выйти',
                      style: TextStyle(fontSize: 18, color: Colors.white)),
                ),
              ),
              Agreement(),
              SizedBox(
                height: 80,
              )
            ],
          ),
        ),
      ),
    );
  }
}
