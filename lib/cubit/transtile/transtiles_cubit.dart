import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:parent_control/data/models/TransUtil.dart';
import 'package:parent_control/data/repository/repository.dart';

part 'transtiles_state.dart';

class TranstilesCubit extends Cubit<TranstilesState> {
  final Repository repository;

  TranstilesCubit({this.repository}) : super(TranstilesInitial());

  void fetchTranstiles(String iin,DateTime date) {
    emit(TranstilesLoading());
    repository.fetchTranstiles(iin,date).then((transUtils){
      print(transUtils);
      emit(TranstilesLoaded(transUtils: transUtils));
    });
  }
}
