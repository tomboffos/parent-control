class Food {
  DateTime date;
  List<dynamic> type;
  String name;
  Food.fromJson(Map json)
      : date = DateTime.parse(json['date']),
        type = json['shoplist'],
        name = json['name'];
}
