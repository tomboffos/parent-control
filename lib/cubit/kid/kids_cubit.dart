import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:parent_control/data/models/kid.dart';
import 'package:parent_control/data/repository/repository.dart';

part 'kids_state.dart';

class KidsCubit extends Cubit<KidsState> {
  final Repository repository;

  KidsCubit({this.repository}) : super(KidsInitial());

  void fetchKids({close = false}) {
    repository.fetchKids().then((kids) {
      emit(KidsLoaded(kids: kids, close: close));
    });
  }

  void showform() {
    repository
        .fetchKids()
        .then((kids) => emit(KidsLoaded(kids: kids, close: true)));
  }

  Future<bool> registerKid(
      BuildContext context, Map<String, dynamic> form) async {
    emit(KidsInitial());
    final close = await repository.registerKid(context, form);
    fetchKids(close: close);
    return close;
  }

  void fetchKid(String iin, bool loading) async {
    if (!loading) emit(KidLoading());

    repository.fetchKid(iin).then((kid) {
      // emit(KidsInitial());
      emit(KidLoaded(
        kid: kid,
      ));
    });
  }

  void updateNotification(Kid kid, String setting, String url, int value) {
    repository
        .updateNotification(kid, setting, url, value)
        .then((response) => fetchKid(kid.iin, true));
  }

  void deleteKid(Kid kid) {
    emit(KidsInitial());
    repository.deleteKid(kid).then((value) => fetchKids());
  }

  void fetchAll(String iin, DateTime dateTime) {
    emit(AllLoading());
    repository
        .fetchAll(iin, dateTime)
        .then((reports) => emit(AllLodaded(reports: reports)));
  }

  void cancelAll() {
    emit(KidsInitial());
    fetchKids();
  }
}
