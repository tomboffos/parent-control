part of 'parent_cubit.dart';

@immutable
abstract class ParentState {}

class ParentInitial extends ParentState {}

class ParentLogged extends ParentState {
  final parentId;

  ParentLogged({this.parentId});
}

class ParentLoginLoading extends ParentState{}


class ParentNotLogged extends ParentState{}
