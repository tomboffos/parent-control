import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:parent_control/cubit/parent/parent_cubit.dart';
import 'package:parent_control/presentation/components/agreements.dart';
import 'package:parent_control/presentation/components/button.dart';
import 'package:parent_control/presentation/components/field.dart';

class ForgetPassword extends StatefulWidget {
  ForgetPassword({Key key}) : super(key: key);

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  bool disabled = false;
  TextEditingController email = new TextEditingController();
  TextEditingController pincode = new TextEditingController();
  TextEditingController password = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () => {},
        child: SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                    image: AssetImage('assets/images/father_and_child.png'),
                    alignment: Alignment.topCenter
                ),
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xff488D9F),
                      Color(0xff2D3245),
                    ])),
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height / 18),
            // margin: EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width - 280,
                      height: MediaQuery.of(context).size.height / 2.8,
                      margin: EdgeInsets.only(bottom: 30),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/images/new_logo.png'),
                              fit: BoxFit.contain),
                          borderRadius: BorderRadius.circular(30)),
                    ),
                    Text(
                      'Родительский\nконтроль',
                      style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 30, left: 20, right: 20),
                    padding: EdgeInsets.symmetric(),
                    child: MainField(
                      controller: email,
                      placeholder: 'Введите почту',
                    )),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: MainField(
                    placeholder: 'Введите код отправленый на почту',
                    keyBoardType: TextInputType.name,
                    controller: pincode,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: MainField(
                    placeholder: 'Введите новый пароль',
                    keyBoardType: TextInputType.name,
                    controller: password,
                    obscureText: true,
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: MainButton(
                    text: 'Получить код',
                    action: !disabled
                        ? () {
                            BlocProvider.of<ParentCubit>(context)
                                .requestPinCode(context, email);
                            setState(() {
                              disabled = true;
                            });
                          }
                        : null,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: MainButton(
                    text: 'Сменить пароль',
                    action: () => BlocProvider.of<ParentCubit>(context)
                        .sendPinCode(context, password, pincode),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: MainButton(
                    text: 'На страницу входа',
                    route: '/login',
                  ),
                ),
                SizedBox(
                  height: 35,
                ),
                Agreement(),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
