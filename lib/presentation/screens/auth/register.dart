import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:parent_control/cubit/parent/parent_cubit.dart';
import 'package:parent_control/presentation/components/agreements.dart';
import 'package:parent_control/presentation/components/choose_buttons.dart';
import 'package:parent_control/presentation/components/field.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController phone =
      new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');
  TextEditingController iin = new MaskedTextController(mask: '000000000000');
  TextEditingController email = new TextEditingController();
  TextEditingController name = new TextEditingController();
  TextEditingController password = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocBuilder<ParentCubit, ParentState>(
        builder: (context, state) {
          if (state is ParentLoginLoading)
            return Center(
              child: CircularProgressIndicator(),
            );
          return GestureDetector(
            onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
            child: SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                        image: AssetImage('assets/images/father_and_child.png'),
                        alignment: Alignment.topCenter
                    ),
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xff488D9F),
                          Color(0xff2D3245),
                        ])),
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 18),
                // margin: EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width - 280,
                          height: MediaQuery.of(context).size.height / 2.8,
                          margin: EdgeInsets.only(bottom: 30),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/images/new_logo.png'),
                                  fit: BoxFit.contain),
                              borderRadius: BorderRadius.circular(30)),
                        ),
                        Text(
                          'Родительский\nконтроль',
                          style: GoogleFonts.montserrat(
                            color: Colors.white,
                            fontSize: 24,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 40,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 12, bottom: 30, left: 20, right: 20),
                      margin: EdgeInsets.only(
                          top: 12, left: 20, right: 20),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Color(0xff0F7764),
                          // boxShadow: [
                          //   BoxShadow(
                          //       offset: Offset(0, 0),
                          //       spreadRadius: 1,
                          //       blurRadius: 10,
                          //       color: Colors.grey),
                          // ],
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Column(
                        children: [
                          Text(
                            'Регистрация родителя',
                            style: GoogleFonts.montserrat(
                                fontSize: 22,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: MainField(
                              placeholder: 'Имя',
                              controller: name,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: MainField(
                              placeholder: 'Номер телефона',
                              controller: phone,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: MainField(
                              controller: email,
                              placeholder: 'Email',
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: MainField(
                              placeholder: 'Пароль',
                              controller: password,
                              obscureText: true,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: MainField(
                              placeholder: 'ИИН',
                              controller: iin,
                            ),
                          ),
                        ],
                      ),
                    ),
                    ChooseButtons(
                      secondText: 'Отменить',
                      firstText: 'Добавить',
                      firstAction: () => BlocProvider.of<ParentCubit>(context)
                          .register(context, {
                        "name": name.text,
                        "phone": phone.text
                            .replaceAll(new RegExp(r'[^\w\s]+'), '')
                            .replaceAll(' ', ''),
                        "password": password.text,
                        "iin": iin.text,
                        "email": email.text
                      }),
                      secondAction: () => Navigator.pop(context),
                    ),
                    SizedBox(
                      height: 46,
                    ),
                    Agreement(),
                    SizedBox(
                      height: 40,
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
