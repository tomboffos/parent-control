import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:parent_control/cubit/parent/parent_cubit.dart';
import 'package:parent_control/presentation/components/agreements.dart';
import 'package:parent_control/presentation/components/button.dart';
import 'package:parent_control/presentation/components/field.dart';

class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController phone =
      new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');
  TextEditingController password = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<ParentCubit, ParentState>(
        builder: (context, state) {
          if (state is ParentLoginLoading)
            return Center(child: CircularProgressIndicator());
          else
            return GestureDetector(
              onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
              child: SingleChildScrollView(
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                          image: AssetImage('assets/images/father_and_child.png'),
                          alignment: Alignment.topCenter
                      ),
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                        Color(0xff488D9F),
                        Color(0xff2D3245),
                      ])),
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height / 18),
                  // margin: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width - 280,
                            height: MediaQuery.of(context).size.height / 2.8,
                            margin: EdgeInsets.only(bottom: 30),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/new_logo.png'),
                                    fit: BoxFit.contain),
                                borderRadius: BorderRadius.circular(30)),
                          ),
                          Text(
                            'Родительский\nконтроль',
                            style: GoogleFonts.montserrat(
                              color: Colors.white,
                              fontSize: 24,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                          margin:
                              EdgeInsets.only(bottom: 30, left: 50, right: 50),
                          padding: EdgeInsets.symmetric(),
                          child: MainField(
                            controller: phone,
                            placeholder: 'Номер телефона',
                            keyBoardType: TextInputType.number,
                          )),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 50),
                        child: MainField(
                          placeholder: 'Пароль',
                          keyBoardType: TextInputType.name,
                          controller: password,
                          obscureText: true,
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 16),
                        child: MainButton(
                          text: 'Войти',
                          action: () => BlocProvider.of<ParentCubit>(context)
                              .login(
                                  phone.text
                                      .replaceAll(new RegExp(r'[^\w\s]+'), '')
                                      .replaceAll(' ', ''),
                                  password.text,
                                  context),
                        ),
                      ),
                      GestureDetector(
                        onTap: ()=>Navigator.pushNamed(context, '/register'),
                        child: Container(
                          margin: EdgeInsets.only(bottom: 16),
                          child:  Text(
                            'Создать аккаунт',
                            style: GoogleFonts.montserrat(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                              fontSize: 16,
                            ),

                            // route: '/register',
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 16),
                        child: MainButton(
                          text: 'Забыли пароль',
                          route: '/forget',
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Agreement(),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            );
        },
      ),
    );
  }
}
