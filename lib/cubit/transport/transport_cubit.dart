import 'package:bloc/bloc.dart';
import 'package:http/http.dart';
import 'package:meta/meta.dart';
import 'package:parent_control/data/models/Transport.dart';
import 'package:parent_control/data/repository/repository.dart';

part 'transport_state.dart';

class TransportCubit extends Cubit<TransportState> {
  final Repository repository;

  TransportCubit({this.repository}) : super(TransportInitial());


  void fetchTransports(iin,DateTime date){
    emit(TransportLoading());
    repository.fetchTransport(iin,date).then((transports)
    {
      emit(TransportLoaded(transports: transports));
    });
  }
}
