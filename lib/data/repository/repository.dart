import 'package:flutter/src/widgets/editable_text.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:parent_control/data/models/Food.dart';
import 'package:parent_control/data/models/TransUtil.dart';
import 'package:parent_control/data/models/Transport.dart';
import 'package:parent_control/data/models/kid.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../network_service.dart';

class Repository {
  final NetworkService networkService;

  Repository({this.networkService});

  Future<List<Kid>> fetchKids() async {
    final kidsJson = await networkService.fetchKids();

    return kidsJson.map((kid) => Kid.fromJson(kid)).toList();
  }

  Future login(String phone, String password, BuildContext context) async {
    final parentId = await networkService.login(phone, password, context);
    return parentId;
  }

  Future checkParentId(BuildContext context) async {
    return (await SharedPreferences.getInstance()).getString('parentId');
  }

  Future register(BuildContext context, Map<String, dynamic> form) async {
    return await networkService.register(context, form);
  }

  Future registerKid(BuildContext context, Map<String, dynamic> form) async {
    return (await networkService.registerKid(context, form));
  }

  Future<Kid> fetchKid(String iin) async {
    return Kid.fromJson(await networkService.fetchKid(iin));
  }

  Future<List<Transport>> fetchTransport(iin, DateTime date) async {
    final transports = await networkService.fetchTransports(iin, date);
    print(transports);
    return transports
        .map((transport) => Transport.fromJson(transport))
        .toList();
  }

  Future<List<TransUtil>> fetchTranstiles(String iin, DateTime date) async {
    final turnstiles = await networkService.fetchTranstiles(iin, date);

    return turnstiles.map((turnTile) => TransUtil.fromJson(turnTile)).toList();
  }

  Future<List<Food>> fetchFoods(String iin, DateTime dateTime) async {
    final foods = await networkService.fetchFoods(iin, dateTime);

    return foods.map((food) => Food.fromJson(food)).toList();
  }

  Future<List<dynamic>> fetchAll(String iin, DateTime dateTime) async {
    return await networkService.fetchAll(iin, dateTime);
  }

  Future updateNotification(
      Kid kid, String setting, String url, int value) async {
    return await networkService.updateNotification(kid, setting, url, value);
  }

  Future requestPinCode(
      BuildContext context, TextEditingController email) async {
    return await networkService.requestPinCode(context, email);
  }

  Future sendPinCode(BuildContext context, TextEditingController password,
      TextEditingController pincode) async {
    return await networkService.sendPinCode(context, password, pincode);
  }

  Future deleteKid(Kid kid) async {
    return await networkService.deleteKid(kid);
  }

// Future<> fetchFood(String iin, DateTime dateTime) {}
}
