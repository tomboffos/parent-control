import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:parent_control/data/models/kid.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkService {
  final apiEndpoint = 'https://shkoda.ml/api';

  Future<List<dynamic>> fetchKids() async {
    print((await SharedPreferences.getInstance()).get('device_token'));
    try {
      final parentId =
          (await SharedPreferences.getInstance()).getString('parentId');
      final response =
          await post(Uri.parse('$apiEndpoint/getchildren'), headers: {
        "Accept": "application/json",
      }, body: {
        "parentid": parentId
      });
      return jsonDecode(response.body)['children'] != null
          ? jsonDecode(response.body)['children'] as List
          : [];
    } catch (e) {
      return [];
    }
  }

  Future login(String phone, String password, BuildContext context) async {
    if (phone.length <= 1) {
      showErrorModal(context, 'Введите телефон');
      return 'Введитя телефон';
    }

    if (password.length < 6) {
      showErrorModal(context, 'Введите пароль');
      return 'Введитя телефон';
    }

    try {
      final device_token =
          (await SharedPreferences.getInstance()).get('device_token');

      final response = await post(Uri.parse('$apiEndpoint/login'),
          headers: {"Accept": "application/json"},
          body: {"phone": phone, "password": password});

      final body = jsonDecode(response.body);

      if (body[0] != null) {
        bool tokenIsFinded = false;
        dynamic id;
        body.forEach((element) {
          if (element['device_token'] == device_token) {
            tokenIsFinded = true;
          }
          id = element['id'];
        });
        if (!tokenIsFinded) await insertToken(id, device_token);

        (await SharedPreferences.getInstance())
            .setString('parentId', body[0]['id'].toString());
        Navigator.pushNamedAndRemoveUntil(context, '/kids', (route) => false);
        return body[0]['id'];
      } else {
        showErrorModal(context, 'Не правильный телефон или пароль');
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future register(BuildContext context, Map<String, dynamic> form) async {
    if (form['name'].length == 0) {
      showErrorModal(context, 'Введите имя');
      return 'Введитя имя';
    }

    if (form['phone'].length <= 1) {
      showErrorModal(context, 'Введите телефон');
      return 'Введитя телефон';
    }

    if (form['phone'].length < 11) {
      showErrorModal(context, 'Телефон должен состоять из 11 цифр');
      return 'Введитя телефон';
    }

    if (form['email'].length == 0) {
      showErrorModal(context, 'Введите почту');
      return 'Введитя почту';
    }

    if (form['iin'].length != 12) {
      showErrorModal(context, 'ИИН должен быть из 12 цифр');
      return 'Введитя почту';
    }

    if (form['password'].length < 6) {
      showErrorModal(context, 'Пароль должен быть больше 6 символов');
      return 'Введитя почту';
    }
    try {
      final device_token =
          (await SharedPreferences.getInstance()).get('device_token');
      final response = await post(Uri.parse('$apiEndpoint/regparent'),
          headers: {"Accept": "application/json"}, body: form);

      final body = jsonDecode(response.body);

      if (body['id'] != null) {
        (await SharedPreferences.getInstance())
            .setString('parentId', body['id'].toString());
        Navigator.pushNamedAndRemoveUntil(context, '/kids', (route) => false);
        print(body);
        await insertToken(body['id'], device_token);
        return body['id'];
      } else {
        showErrorModal(context, body['message']);
        return body['message'];
      }
    } catch (e) {
      print(e);
    }
  }

  Future registerKid(BuildContext context, Map<String, dynamic> form) async {
    if (form['name'].length == 0) {
      showErrorModal(context, 'Введите имя');
      return true;
    }
    if (form['iin'].length != 12) {
      showErrorModal(context, 'ИИН должен быть из 12 цифр');
      return true;
    }
    form['parentid'] = (await SharedPreferences.getInstance()).get('parentId');
    if (form['id'] != null)
      return await updateKid(context, form);
    else
      form.remove('id');
    final response = await post(Uri.parse('$apiEndpoint/regchild'), body: form);

    final body = jsonDecode(response.body);
    if (body['error'] != null)
      showErrorModal(context, body['error']);
    else
      showSuccessModel(context, 'Ребенок был добавлен');
    return false;
  }

  Future updateKid(BuildContext context, Map<String, dynamic> form) async {
    final response =
        await post(Uri.parse('$apiEndpoint/updateuser'), body: form);
    final body = jsonDecode(response.body);
    if (body['message'] != null)
      showSuccessModel(context, body['message']);
    else
      showSuccessModel(context, 'Ребенок был Обновлен');
    return false;
  }

  Future<Map<String, dynamic>> fetchKid(String iin) async {
    return jsonDecode(
        (await post(Uri.parse('$apiEndpoint/getchild'), body: {"iin": iin}))
            .body)['childData'];
  }

  Future<List<dynamic>> fetchTranstiles(String iin, DateTime date) async {
    try {
      final response = await post(Uri.parse('$apiEndpoint/getturnstiles'),
          body: {"iin": iin, "date": "${date.year}-${date.month}-${date.day}"});
      print(response.body);
      return jsonDecode(response.body)['turnstiles'] as List;
    } catch (e) {
      return [];
    }
  }

  Future<List<dynamic>> fetchTransports(iin, DateTime date) async {
    try {
      final response = await post(Uri.parse('$apiEndpoint/gettransport'),
          body: {"iin": iin, "date": "${date.year}-${date.month}-${date.day}"});

      return jsonDecode(response.body)['transport'] as List;
    } catch (e) {
      return [];
    }
  }

  Future<List<dynamic>> fetchFoods(String iin, DateTime dateTime) async {
    try {
      print(dateTime);
      final response = await post(Uri.parse('$apiEndpoint/getfood'), body: {
        "iin": iin,
        "date": "${dateTime.year}-${dateTime.month}-${dateTime.day}"
      });
      print(response.body);

      return jsonDecode(response.body)['food'] as List;
    } catch (e) {
      return [];
    }
  }

  Future updateNotification(
      Kid kid, String setting, String url, int value) async {
    final response = await post(Uri.parse('$apiEndpoint/$url'),
        body: {"iin": kid.iin, "$setting": "$value"});
    print(response.body);
    return jsonDecode(response.body)['message'];
  }

  Future<List<dynamic>> fetchAll(String iin, DateTime dateTime) async {
    final response = await post(Uri.parse('$apiEndpoint/getall'), body: {
      "iin": iin,
      "date": "${dateTime.year}-${dateTime.month}-${dateTime.day}"
    });
    print(jsonDecode(response.body));

    return jsonDecode(response.body)['all'];
  }

  Future requestPinCode(
      BuildContext context, TextEditingController email) async {
    if (email.text.length == 0) {
      showErrorModal(context, 'Введите почту');
      return 'Введите почту';
    }
    final response = await post(Uri.parse('$apiEndpoint/getcode'),
        body: {"email": email.text});

    if (jsonDecode(response.body)['message'] == 'Ok')
      showSuccessModel(context, jsonDecode(response.body)['message']);
    else
      showErrorModal(context, jsonDecode(response.body)['message']);
    return jsonDecode(response.body)['message'];
  }

  Future sendPinCode(BuildContext context, TextEditingController password,
      TextEditingController pincode) async {
    if (password.text.length < 6) {
      showErrorModal(
          context, 'Пароль должен состоять из 6 символов или больше');
      return false;
    }

    if (pincode.text.length == 0) {
      showErrorModal(context, 'Введите пинкод');
      return false;
    }
    final response = await post(Uri.parse('$apiEndpoint/setnewpassword'),
        body: {"code": pincode.text, "password": password.text});
    print(jsonDecode(response.body));
    if (jsonDecode(response.body)['error'] != null)
      showErrorModal(context, 'Вы ввели не верный код');
    else {
      (await SharedPreferences.getInstance()).setString(
          'parentId', jsonDecode(response.body)['userId'].toString());
      showSuccessModel(context, 'Успешно пароль изменен',
          function: () => Navigator.pushNamedAndRemoveUntil(
              context, '/kids', (route) => false));
    }
    return jsonDecode(response.body)['message'];
  }

  Future<void> insertToken(id, deviceToken) async {
    final response = await post(Uri.parse('$apiEndpoint/inserttoken'),
        body: {"id": id.toString(), "token": deviceToken});
    print(response.body);
  }

  Future deleteKid(Kid kid) async {
    final response = await post(Uri.parse('$apiEndpoint/deleteuser'),
        body: {"iin": kid.iin});
  }
}

showErrorModal(BuildContext context, String message) {
  AwesomeDialog(
    context: context,
    dialogType: DialogType.ERROR,
    animType: AnimType.BOTTOMSLIDE,
    title: 'Ошибка',
    desc: message,
    btnOkOnPress: () {},
  )..show();
}

showSuccessModel(BuildContext context, String message, {function}) {
  AwesomeDialog(
    context: context,
    dialogType: DialogType.SUCCES,
    animType: AnimType.BOTTOMSLIDE,
    title: 'Успешно',
    desc: message,
    btnOkOnPress: function != null ? function : () => {},
  )..show();
}
