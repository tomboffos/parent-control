part of 'transport_cubit.dart';

@immutable
abstract class TransportState {}

class TransportInitial extends TransportState {}
class TransportLoading extends TransportState {}
class TransportLoaded extends TransportState {
  final List<Transport> transports;

  TransportLoaded({this.transports});
}